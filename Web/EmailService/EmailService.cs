﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
/*** FB 1838 Service manager changes ***/

#region References
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Net;
using System.IO;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using msxml4_Net;
using System.Data;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Collections.Generic;
using System.ServiceProcess;
using ns_SqlHelper; 
#endregion

namespace EmailService
{
    public partial class EmailService : ServiceBase
    {
        private static DataTable _dtConfEPTs = null;
        static String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        static String MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
        static String COM_ConfigPath = dirPth + "\\VRMSchemas\\COMConfig.xml";
        static String RTC_ConfigPath = dirPth + "\\VRMSchemas\\EmailConfig.xml";
        static Email.Email obj = null;
        System.Timers.Timer timerLaunchEmails = new System.Timers.Timer();
        static System.Timers.Timer timerAutoPurgelogs = new System.Timers.Timer();//ZD 104846

        static NS_CONFIG.Config config = null;
        static NS_MESSENGER.ConfigParams configParams = null;
        static string configPath = dirPth + "\\VRMMaintServiceConfig.xml";
        static string errMsg = null;
        static NS_LOGGER.Log log = null;
        static bool ret = false;
        static bool stop = false;
        static ASPIL.VRMServer myvrmCom = null;//ZD 104846
        static int purgeDuration = 0;//ZD 104846

        public EmailService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            double mailLaunch = 30000;
            double PurgeLogsInterval = 24 * 60 * 60 * 1000; //ZD 104846 //24 Hours
            
            try
            {
                stop = false;
                config = new NS_CONFIG.Config();
                configParams = new NS_MESSENGER.ConfigParams();
                ret = config.Initialize(configPath, ref configParams, ref errMsg, MyVRMServer_ConfigPath, RTC_ConfigPath);
                log = new NS_LOGGER.Log(configParams);

                log.Trace("Into The service started");
                log.Trace("Various Configs COM:" + COM_ConfigPath + " RTC:" + RTC_ConfigPath + " ASPIL:" + MyVRMServer_ConfigPath);
                                

                timerLaunchEmails.Elapsed += new System.Timers.ElapsedEventHandler(timerLaunchEmails_Elapsed);
                timerLaunchEmails.Interval = mailLaunch;
                timerLaunchEmails.AutoReset = false;
                timerLaunchEmails.Start();

                //ZD 104846 start
                timerAutoPurgelogs.Elapsed += new System.Timers.ElapsedEventHandler(timerAutoPurgelogs_Elapsed);
                timerAutoPurgelogs.Interval = PurgeLogsInterval;
                timerAutoPurgelogs.Enabled = true;
                timerAutoPurgelogs.Start();
                //ZD 104846 End
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }


        }

        

        static void timerLaunchEmails_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {

                MiscThread();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }



        #region MiscThread
        //private void MiscThread(Object stateInfo)
        private static void MiscThread()
        {
            try
            {
                while (!stop)
                {

                    log.Trace("Into Mail:" + DateTime.Now.ToString("F"));
                    obj = new Email.Email();
                    obj.Operations(RTC_ConfigPath, "SendEmails", "");
                    Thread.Sleep(5000);
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }

        #endregion

      

        protected override void OnStop()
        {
            stop = true;
            timerLaunchEmails.Enabled = false;
            timerLaunchEmails.AutoReset = false;
            timerLaunchEmails.Stop();
        }


        //ZD 104846 start
        #region timerAutoPurgelogs_Elapsed
        /// <summary>
        /// timerAutoPurgelogs_Elapsed
        /// </summary>
        void timerAutoPurgelogs_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                log.Trace("Purge Logs:" + DateTime.Now.ToString("F"));
                timerAutoPurgelogs.AutoReset = false;
                GetSitePurgeLogDuartion();
                PurgeLogs();
                System.Threading.Thread.Sleep(5000);
                timerAutoPurgelogs.AutoReset = true; ;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }

        #endregion

        #region GetSitePurgeLogDuartion
        /// <summary>
        /// GetSitePurgeLogDuartion()
        /// </summary>
        private void GetSitePurgeLogDuartion()
        {
            XmlDocument xmldoc = new XmlDocument();
            string stmt = "", schemapath = "";
            try
            {
                log.Trace("Into GetSitePurgeLogDuartion... " + DateTime.Now.ToLocalTime());
                schemapath = "C:\\VRMSchemas_v1.8.3\\";
                ns_SqlHelper.SqlHelper sqlCon = new ns_SqlHelper.SqlHelper(schemapath);
                sqlCon.OpenConnection();
                stmt = "select AutoPurgeLogDuration from Sys_Settings_D";
                System.Data.DataSet ds = sqlCon.ExecuteDataSet(stmt);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        int.TryParse(ds.Tables[0].Rows[0]["AutoPurgeLogDuration"].ToString(), out purgeDuration);
                    }
                }
                sqlCon.CloseConnection();
            }
           
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        #region PurgeLogs
        /// <summary>
        /// PurgeLogs
        /// </summary>
        private void PurgeLogs()
        {
            string[] Files = null;
            try
            {
                if (Directory.Exists(MyVRMServer_ConfigPath + "\\MaintenanceLogs"))
                {
                    Files = Directory.GetFiles(MyVRMServer_ConfigPath + "\\MaintenanceLogs");
                    for (int i = 0; i < Files.Length; i++)
                    {
                        FileInfo fi = new FileInfo(Files[i]);
                        if (DateTime.UtcNow - fi.CreationTimeUtc > TimeSpan.FromDays(purgeDuration))
                            fi.Delete();

                    }
                }
                if (Directory.Exists(MyVRMServer_ConfigPath + "\\EmailLogs"))
                {
                    Files = Directory.GetFiles(MyVRMServer_ConfigPath + "\\EmailLogs");
                    for (int i = 0; i < Files.Length; i++)
                    {
                        FileInfo fi = new FileInfo(Files[i]);
                        if (DateTime.UtcNow - fi.CreationTimeUtc > TimeSpan.FromDays(purgeDuration))
                            fi.Delete();
                    }
                }

                log.Trace("Deleted Email Service Logs");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        //ZD 104846 End
    }
}
